#!/bin/bash

cd $(dirname $0)

# Update Gotify
docker-compose pull
docker-compose up -d
if [ $? -ne 0 ]; then
    exit 1;
fi
date=$(date)
echo "[$date]" "Gotify updated"
